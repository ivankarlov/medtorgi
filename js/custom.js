/*jslint browser: true*/
/*jslint regexp: true */
/*jslint nomen: true*/
/*global $, jQuery, alert, console, google, Modernizr*/

var str;


//Toggle Text

$.fn.extend({
  toggleText: function (a, b) {
    "use strict";
    var that = this;
    if (that.text() !== a && that.text() !== b) {
      that.text(a);
    } else if (that.text() === a) {
      that.text(b);
    } else if (that.text() === b) {
      that.text(a);
    }
    return this;
  }
});

//Toggle Prop
$.fn.toggleProp = function (prop) {
  "use strict";
  return this.each(function () {
    var $this = $(this);
    $this.prop(prop) ? $this.prop(prop, false) : $this.prop(prop, true);
  });
};

window.reset = function (e) {
  "use strict";
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
};

$(function () {
  'use strict';
  //Placeholder
  if (!Modernizr.input.placeholder) {

    $('[placeholder]').focus(function () {
      var input = $(this);
      if (input.val() === input.attr('placeholder')) {
        input.val('');
        input.removeClass('placeholder');
      }
    }).blur(function () {
      var input = $(this);
      if (input.val() === '' || input.val() === input.attr('placeholder')) {
        input.addClass('placeholder');
        input.val(input.attr('placeholder'));
      }
    }).blur();
    $('[placeholder]').parents('form').submit(function () {
      $(this).find('[placeholder]').each(function () {
        var input = $(this);
        if (input.val() === input.attr('placeholder')) {
          input.val('');
        }
      });
    });

  }

  //Mobile optimisation of full version

  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('html').addClass('mobile');
  }


  //Popup
  $('body').on('click', '.popup-link', function (e) {
    e.preventDefault();
    str = this.href.split("#")[1];
    $('#' + str).bPopup({
      opacity: 0.6,
      transition: 'fadeIn',
      transitionClose: 'slideDown',
      onOpen: function () {
        $(this).attr("aria-hidden", false);
        $('input:radio, input:file, input:checkbox, select').trigger("refresh");
      },
      onClose: function () {
        $(this).attr("aria-hidden", true);
      }
    });
  });


  //Tabs
  if ($('.tabs').length) {
    $('.main-tabs').tabs({
      active: 0,
      activate: function (event, ui) {
        $('.tab-imit').toggleClass('active');
      }
    });
    $('.tabs').tabs({
      active: 0
    });
  }

  //css multicolumns

  if (!Modernizr.csscolumns) {
    $('.adv-category-ul').autocolumnlist({
      columns: 2,
      classname: 'adv-list-column',
      min: 1
    });
    $('.footer-nav ul').autocolumnlist({
      columns: 3,
      classname: 'footer-list-column',
      min: 1
    });
  }

  //Slider

  $('.partners-carousel').slick({
    slidesToShow: 6,
    slidesToScroll: 6,
    dots: true,
    infinite: false
  });


  //Checkbox

  $('input:radio, input:file, input:checkbox, select').styler();

  //Timer
  var countDownTime = parseInt($('#timer').data('time'), 10);

  function countDownTimer() {
    var hours = parseInt(countDownTime / 3600, 10),
      minutes = parseInt(countDownTime / 60, 10) % 60,
      seconds = countDownTime % 60,
      result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + " ‘" + (seconds < 10 ? "0" + seconds : seconds);
    $('#timer').html(result);
    if (countDownTime === 0) {
      countDownTime = 60 * 60 * 60;
    }
    countDownTime = countDownTime - 1;
    setTimeout(function () {
      countDownTimer();
    }, 1000);
  }
  countDownTimer();

  //Fancybox

  if ($("a").is(".fancybox")) {
    $(".fancybox").fancybox({
      helpers: {
        overlay: {
          locked: false // important!!
        }
      }
    });
  }


  //range slider
  if ($(".range").length) {
    $(".range").ionRangeSlider({
      hide_min_max: true,
      hide_from_to: true,
      onStart: function (data) {
        data.input.parent().siblings('.range-from-num').text(data.from);
      },
      onChange: function (data) {
        data.input.parent().siblings('.range-from-num').text(data.from);
      }
    });
  }

  $('.dig-input').on('input change', function () {
    $(this).val($(this).val().replace(/\D/, ''));
  });

  $('.price-input').on('input change', function () {
    $(this).val($(this).val().replace(/^\.|\d{1,4}\..*\.|[^\d\.{1}]/, ''));
  });

  //Datepickers

  $.datepicker.regional.ru = {
    closeText: 'Закрыть',
    prevText: '&#x3C;Пред',
    nextText: 'След&#x3E;',
    currentText: 'Сегодня',
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
    dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
    dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    weekHeader: 'Нед',
    dateFormat: 'dd.mm.y',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
  };
  $.datepicker.setDefaults($.datepicker.regional.ru);

  $(".begin-date").datepicker({
    prevText: '',
    nextText: '',
    onSelect: function (selectedDate) {
      $(this).closest('.date-range').find('.end-date').datepicker("option", "minDate", $(this).datepicker('getDate'));
      this.focus();
    }
  });

  $(".end-date").datepicker({
    prevText: '',
    nextText: '',
    onSelect: function (selectedDate) {
      $(this).closest('.date-range').find('.begin-date').datepicker("option", "maxDate", $(this).datepicker('getDate'));
      this.focus();
    }
  });


  //Spinner

  if ($('.spinner').length) {

    $(".spinner").on("spincreate spinstop", function () {
      var val = parseInt(this.value, 10);
      if (val === $(this).spinner('option', 'max')) {
        $(this).siblings('.ui-spinner-up').addClass('ui-spin-disabled');
      } else if (val === $(this).spinner('option', 'min')) {
        $(this).siblings('.ui-spinner-down').addClass('ui-spin-disabled');
      } else {
        $(this).siblings('.ui-spinner-button').removeClass('ui-spin-disabled');
      }
    }).spinner({
      min: 0,
      max: 1000
    }).on('input', function () {
      var val = this.value,
        $this = $(this),
        max = $this.spinner('option', 'max'),
        min = $this.spinner('option', 'min');
      if (!val.match(/^\d+$/)) {
        val = 0;
      }
      this.value = val > max ? max : val < min ? min : val;
    });
  }


  $('.category-ul li').each(function () {
    if ($(this).children('ul').length) {
      $(this).prepend('<i class="icon toggle-icon"></i>');
    }
  });

  $('body').on('click', '.toggle-icon', function () {
    $(this).toggleClass('active');
    $(this).parent().children('ul').slideToggle();
  });

  $('.price-range-input').on('change', function () {

    var $min_val = parseInt($(this).siblings('.price-min-value').val(), 10),
      $max_val = parseInt($(this).siblings('.price-max-value').val(), 10),
      $val = parseInt($(this).val(), 10);

    if ($(this).hasClass('price-min-value') && $val > $max_val) {
      $(this).val('');
    } else if ($(this).hasClass('price-max-value') && $val < $min_val) {
      $(this).val('');
    }
  });


  //Onclick events

  $('.like-box').on('click', function () {
    $(this).toggleClass('active');
  });

  $('.reset-link').on('click', function () {
    $('select').prop('selectedIndex', 0).trigger('refresh');
  });

  $('.up-box').on('click', function () {
    $('html, body').animate({
      scrollTop: 0
    }, 1000);
  });

  $('.tariff-column-inner').on('click', function () {
    $('.tariff-column-inner').removeClass('active');
    $(this).addClass('active');
  });

  $('.all-categories').on('click', function () {

    var ul_height = $('.equip-toggle-box-inner ul').outerHeight(true),
      box_height = 287;

    if (!$('#toggle-category-list').hasClass('expanded')) {
      $(this).attr('aria-expanded', true).addClass('active');
      $('#toggle-category-list').animate({
        'height': ul_height
      }, function () {
        $(this).addClass('expanded');
      });
    } else {
      $(this).attr('aria-expanded', false).removeClass('active');
      $('#toggle-category-list').animate({
        'height': box_height
      }, function () {
        $(this).removeClass('expanded');
      });
    }
  });

  $('.toggle-categories').on('click', function () {

    var ul_height = $(this).prev('.sidebar-toggle-box').children('ul').outerHeight(true),
      box_height = 134;

    if (!$(this).prev('.sidebar-toggle-box').hasClass('expanded')) {
      $(this).attr('aria-expanded', true).addClass('active').text('Скрыть список');
      $(this).prev('.sidebar-toggle-box').animate({
        'height': ul_height
      }, function () {
        $(this).addClass('expanded');
      });
    } else {
      $(this).attr('aria-expanded', false).removeClass('active').text('Показать всё');
      $(this).prev('.sidebar-toggle-box').animate({
        'height': box_height
      }, function () {
        $(this).removeClass('expanded');
      });
    }
  });

  $('.sale-ul a').on('click', function () {
    $(this).parent('li').find('input:radio').trigger('click');
    return false;
  });

  $('.tag-filter-ul a').not('.remove-li').on('click', function () {
    $(this).toggleClass('active');
    return false;
  });

  $('input.toggle-all-cb').on('change', function () {
    if ($(this).prop('checked') === true) {
      $(this).parents('.adv-filter-bar').next().find('input:checkbox').prop('checked', true).trigger('refresh');
    } else {
      $(this).parents('.adv-filter-bar').next().find('input:checkbox').prop('checked', false).trigger('refresh');
    }
  });

  $('.delete-row').on('click', function () {
    $(this).parents('tr').remove();
  });

  $('.sort-ul a').on('click', function () {
    if ($(this).hasClass('active')) {
      $(this).toggleClass('up');
    }
  });

  $('.registry-ul a').on('click', function () {
    $(this).closest('.registry-ul').find('a').removeClass('active');
    $(this).addClass('active');
  });



  //Expander

  $('.company-box-descr-inner').expander({
    slicePoint: 363,
    expandText: 'Читать полностью',
    userCollapseText: 'Свернуть',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut',
    expandAfterSummary: true
  });

  $('.region-ul-expander').expander({
    slicePoint: 230,
    expandText: 'Показать все регионы доставки',
    userCollapseText: 'Скрыть регионы доставки',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut',
    expandAfterSummary: true
  });

  $('.item-region-ul-expander').expander({
    slicePoint: 230,
    expandText: 'Развернуть',
    userCollapseText: 'Свернуть',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut',
    expandAfterSummary: true
  });

  $('.question-box-expander').expander({
    slicePoint: 222,
    expandText: 'Развернуть',
    userCollapseText: 'Свернуть',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut',
    expandAfterSummary: true
  });

  $('.review-content').expander({
    slicePoint: 222,
    expandText: 'Развернуть',
    userCollapseText: 'Свернуть',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut',
    expandAfterSummary: true
  });

  $('.tech-expander').expander({
    slicePoint: '100000',
    sliceOn: '</dl>',
    expandText: 'Показать все характеристики',
    userCollapseText: 'Свернуть характеристики',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut',
    expandPrefix: '',
    expandAfterSummary: true
  });

  $('.expander-item-descr').expander({
    slicePoint: 375,
    expandText: 'Развернуть',
    userCollapseText: 'Свернуть',
    expandEffect: 'fadeIn',
    collapseEffect: 'fadeOut'
  });

  //ellipsis

  $(".item-eq-ul figcaption").dotdotdot({});

  //New advertisment photo upload

  $('body').on('change', '.photo-upload', function () {
    if (!$(this).parents('.photo-upload-box').find('.delete-upload').length || $('.upload-wrapper').find('input:file').length === 1) {
      $(this).parents('.upload-wrapper').append('<div class="photo-upload-box"><input class="photo-upload" type="file" data-browse=" "></div>');
      $('input:file').styler();
      $(this).parents('.photo-upload-box').append('<a class="delete-upload" href="#">X</a>');
      return false;
    }
  });

  $('body').on('click', '.delete-upload', function () {
    var input = $(this).parent().find('input:file');
    input.replaceWith(input.val('').clone(true)).trigger('refresh');
    if ($('.upload-wrapper').find('input:file').length > 1) {
      $(this).parent().remove();
    }
    return false;
  });

  $(document).ready(function () {
    $('.inner-box-ul li').each(function () {
      $('.offer-list-item-price', this).text($('.offer-list-item-price', this).text().replace(/\s/g, "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 "));
    });
    $('.offers-ul li').each(function () {
      $('.offer-list-item-price', this).text($('.offer-list-item-price', this).text().replace(/\s/g, "").replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 "));
    });
  });

  $('#reg_new_user_form .submit').on('click', function () {
    var data = $('#reg_new_user_form').serialize();
    if ($('#agreement').prop('checked')) {
      $.post(
        '/ajax/reg_new_user.php',
        data,
        function (res) {
          if ($.isNumeric(res)) {
            console.log(res);
          } else {
            alert(res);
          }
        }
      );
    } else {
      alert('Вы должны согласиться с пользовательским соглашением!');
    }
  });

  $('.add-lot').on('click', function () {
    var $tr = $(this).parent().prev().find('tr').last().clone();
    $tr.find('.small-input').val('');
    $(this).parent().prev().find('tbody').append($tr);

  });

});
